# -*- coding: utf-8 -*-

# imports
from tensorflow.keras.applications import VGG16, VGG19, EfficientNetB0, EfficientNetB1
from tensorflow.keras.applications import ResNet50V2, ResNet101V2, InceptionResNetV2

from tensorflow.keras.layers import Dense, Dropout, BatchNormalization, LeakyReLU
from tensorflow.keras.layers import Flatten, MaxPooling2D, AveragePooling2D
from tensorflow.keras.models import Model

def vgg16():
    base_model = VGG16(include_top=False,
                         weights='imagenet',
                         input_shape=(32,32,3),
                         classes = 10)
    head_model = base_model.output
    #head_model = AveragePooling2D(pool_size=(4, 4), padding = 'same')(head_model)
    head_model = Flatten(name="flatten")(head_model)
    head_model = Dense(1024, activation="relu")(head_model)
    head_model = Dense(512, activation="relu")(head_model)
    head_model = Dropout(0.3)(head_model)
    head_model = Dense(128, activation="relu")(head_model)
    #head_model = Dropout(0.2)(head_model)

    head_model = Dense(10, activation="softmax")(head_model)
    
    model = Model(inputs = base_model.input, outputs = head_model)
    
    return model

def vgg19():
    base_model = VGG19(include_top=False,
                         weights='imagenet',
                         input_shape=(32,32,3),
                         classes = 10)
    head_model = base_model.output
    #head_model = AveragePooling2D(pool_size=(4, 4), padding = 'same')(head_model)
    head_model = Flatten(name="flatten")(head_model)
    head_model = Dense(1024)(head_model)
    head_model = LeakyReLU(0.2)(head_model)
    head_model = Dense(512)(head_model)    
    head_model = LeakyReLU(0.2)(head_model)
    head_model = Dropout(0.3)(head_model)

    head_model = Dense(10, activation="softmax")(head_model)
    
    model = Model(inputs = base_model.input, outputs = head_model)
    
    return model

def effb0():
    base_model = EfficientNetB0(include_top=False,
                         weights='imagenet',
                         input_shape=(32,32,3),
                         classes = 10)
    head_model = base_model.output
    #head_model = BatchNormalization()(head_model)
    head_model = Flatten()(head_model)

    head_model = Dense(10, activation="softmax")(head_model)
    
    model = Model(inputs = base_model.input, outputs = head_model)
    
    return model

def effb1():
    base_model = EfficientNetB1(include_top=False,
                         weights='imagenet',
                         input_shape=(32,32,3),
                         classes = 10)
    head_model = base_model.output
    #head_model = BatchNormalization()(head_model)
    head_model = Flatten()(head_model)
    
    head_model = Dense(64, activation="relu")(head_model)
    #head_model = Dropout(0.5)(head_model)

    head_model = Dense(10, activation="softmax")(head_model)
    
    model = Model(inputs = base_model.input, outputs = head_model) 
    
    for layer in base_model.layers:
        layer.trainable = False
    
    return model

def res50v2():
    base_model = ResNet50V2(include_top=False,
                         weights='imagenet',
                         input_shape=(32,32,3),
                         classes = 10)
    head_model = base_model.output
    #head_model = BatchNormalization()(head_model)
    head_model = Flatten()(head_model)
    
    head_model = Dense(64, activation="relu")(head_model)
    #head_model = Dropout(0.5)(head_model)

    head_model = Dense(10, activation="softmax")(head_model)
    
    model = Model(inputs = base_model.input, outputs = head_model)
    
    for layer in base_model.layers:
        layer.trainable = False
    
    return model

def inception():
    base_model = InceptionResNetV2(include_top=False,
                         weights='imagenet',
                         input_shape=(32,32,3),
                         classes = 10)
    head_model = base_model.output
    #head_model = BatchNormalization()(head_model)
    head_model = Flatten()(head_model)
    
    head_model = Dense(512, activation="relu")(head_model)
    #head_model = Dropout(0.5)(head_model)

    head_model = Dense(10, activation="softmax")(head_model)
    
    model = Model(inputs = base_model.input, outputs = head_model)
    
    for layer in base_model.layers:
        layer.trainable = False
    
    return model

def res101v2():
    base_model = ResNet101V2(include_top=False,
                         weights='imagenet',
                         input_shape=(32,32,3),
                         classes = 10)
    head_model = base_model.output
    #head_model = AveragePooling2D(pool_size=(4, 4), padding = 'same')(head_model)
    head_model = Flatten(name="flatten")(head_model)
    head_model = Dense(1024, activation = 'relu')(head_model)
    #head_model = LeakyReLU(0.2)(head_model)
    head_model = Dense(512, activation = 'relu')(head_model)    
    #head_model = LeakyReLU(0.2)(head_model)
    head_model = Dropout(0.3)(head_model)

    head_model = Dense(10, activation="softmax")(head_model)
    
    model = Model(inputs = base_model.input, outputs = head_model)
    
    return model



