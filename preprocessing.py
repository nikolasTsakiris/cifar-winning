#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:01:14 2021

@author: nikolai
"""

#imports
import tensorflow as tf
#import seaborn as sns
import numpy as np
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.preprocessing.image import load_img, img_to_array
#import matplotlib.pyplot as plt
#import matplotlib.image as mpimg
#import itertools
tf.random.set_seed(42)

#function to compress each value to the [0.0, 1.0] range
"""
Since images belong to the RGB format, each one of them has 
a value between 0.0 and 255.0. Thus, normalization happens via
division with 255.0 (ex. 47/255 = ~0.18). Furthermore, our first ConvNet layer
expects an input of size (ALL_IMAGE_SUM, m, n, 3), so we reshape accordingly.
"""
#function to reshape AND normalize
def normalization(xtrain, xval, xtest):
    xtrain = xtrain.reshape(xtrain.shape[0],#all 50000 images
                            xtrain.shape[1],#width of 32
                            xtrain.shape[2],#height of 32
                            3)#color channel
    xtrain = xtrain / 255.0#normalize
    
    xval = xval.reshape(xval.shape[0],#all 50000 images
                            xval.shape[1],#width of 32
                            xval.shape[2],#height of 32
                            3)#color channel
    xval = xval / 255.0#normalize
    
    xtest = xtest.reshape(xtest.shape[0],#all 50000 images
                          xtest.shape[1],#width of 32
                          xtest.shape[2],#height of 32
                          3)#color channel
    xtest = xtest / 255.0#normalize 
    
    
    return xtrain, xval, xtest

#function to one-hot encode our labels
def one_hot_babe(ytrain, yval, ytest):
    ytrain = ytrain.flatten()
    yval = yval.flatten()
    ytest = ytest.flatten()
    
    ytrain = tf.one_hot(ytrain.astype(np.int32), depth=10) 
    yval = tf.one_hot(yval.astype(np.int32), depth=10)
    ytest = tf.one_hot(ytest.astype(np.int32), depth=10)
    
    return ytrain, yval, ytest

def augmentator(xtrain, ytrain, xval, yval, batch_size):
    datagen = ImageDataGenerator(
                            shear_range = 0.35,
                            zoom_range = 0.2,
                            horizontal_flip = True,
                            vertical_flip = True,
                            fill_mode="nearest",
                            width_shift_range=0.25,
                            height_shift_range=0.25,
                            rotation_range=30,
                            #brightness_range = [0.2, 1], 
                            #zca_whitening = True
                              )
    
    iterator_train = datagen.flow(xtrain, ytrain, batch_size = batch_size)
    iterator_val = datagen.flow(xval, yval, batch_size = batch_size)  

    
    return iterator_train, iterator_val

#predict an image
def image_preparator(image):
    img = load_img(image, target_size = (32, 32))
    img = img_to_array(img)
    img = img.reshape(1, 32, 32, 3)
    img = img.astype(np.int32)
    img = img / 255.0
    return img

def predict_image(model):
    img = image_preparator('./sample_image-1.png')
    verdict = model.predict(img)
    y_classes = verdict.argmax(axis=-1)
    print(y_classes)










