#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:39:47 2021

@author: nikolai
"""
import os
import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout, Flatten, Input, MaxPooling2D, AveragePooling2D
from tensorflow.keras.layers import Conv2D, BatchNormalization, ReLU
from tensorflow.keras import Model


"""HYPERPARAMS = {
    'xception':{
        'epochs':20,
        'img_size':32,
        'batch_size':16,
        'fine-tune_epochs':40,
        'fine_tune_initial-unfrozen-layer':320
    },
    'efficientnet4':{
        'img_size':300,
        'epochs':20,
        'batch_size':16,
        'fine-tune_epochs':40,
        'fine_tune_initial-unfrozen-layer':200
    },
    'efficientnet7':{
        'epochs':20,
        'batch_size':16,
        'img_size':32,
        'fine-tune_epochs':40,
        'fine_tune_initial-unfrozen-layer':200
    }
}"""


def generic():
    
    inputs = Input(shape = (32, 32, 3))
    filters = 16,
    kernel_init = 'he_uniform'
    padding = 'same',
    drop = 0.1
    classes = 10

    #create CNN
    x = generic_block(inputs, 16, drop*0)
    x = generic_block(x, 32, drop*0)
    x = generic_block(x, 64, drop*0)
    x = generic_block(x, 128, drop*0)
    x = generic_block(x, 256, drop*0)
    x = generic_block(x, 512, drop*0)

    outputs = classifier(x, classes)
    
    
    model = Model(inputs, outputs)
    
    return model

def generic_block(x, filters, drop):
    
    x = Conv2D(filters, (1, 1), strides=(1, 1), padding= 'same', kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    x = ReLU()(x)
    x = Conv2D(filters, (3, 3), strides=(1, 1), padding= 'same', kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    x = ReLU()(x)
    x = Conv2D(filters, (1, 1), strides=(1, 1), padding= 'same', kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    x = ReLU()(x)
    x = AveragePooling2D(padding= 'same')(x) 
    x = Dropout(drop)(x)
    
    return x

def classifier(x, classes):
    # Flatten into 1D vector 
    x = Flatten()(x)
    
    x = Dense(1024 , activation = 'relu', kernel_initializer = 'he_normal')(x)
    #x = Dropout(0.5)(x)
    
    # Final Dense Outputting Layer 
    outputs = Dense(classes, activation='softmax', kernel_initializer = 'he_normal')(x)
    
    return outputs
#-----------------------------------------------------------------------------


    
    
    
    
    
    
    
    
    
    